<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2025-01-22 08:51:05 by cardo-->
<display version="2.0.0">
  <name>PermBcpSuctionLine</name>
  <width>920</width>
  <widget type="rectangle" version="2.0.0">
    <name>Titlebar</name>
    <class>TITLE-BAR</class>
    <x use_class="true">0</x>
    <y use_class="true">0</y>
    <width>920</width>
    <height use_class="true">50</height>
    <line_width use_class="true">0</line_width>
    <background_color use_class="true">
      <color name="PRIMARY-HEADER-BACKGROUND" red="151" green="188" blue="202">
      </color>
    </background_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BcpSuctionLine</name>
    <class>TITLE</class>
    <text>BCP Suction Line Vented</text>
    <x use_class="true">20</x>
    <y use_class="true">0</y>
    <width>390</width>
    <height use_class="true">50</height>
    <font use_class="true">
      <font name="Header 1" family="Source Sans Pro" style="BOLD_ITALIC" size="36.0">
      </font>
    </font>
    <foreground_color use_class="true">
      <color name="HEADER-TEXT" red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <transparent use_class="true">true</transparent>
    <horizontal_alignment use_class="true">0</horizontal_alignment>
    <vertical_alignment use_class="true">1</vertical_alignment>
    <wrap_words use_class="true">false</wrap_words>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>1) Close HV-42111 (SP compressor inlet) and HV-42112 (HP compressor inlet).

2) Close suction valve HV-91000 and discharge valve HV-91200.

3) Release BCP internal pressure via HV-91202 to oil contaminated safety relief header to suction pressure, i.e. until PI-91010 = PI-91003.

4) Open PV-91000 from Aerzen HMI.

5) Carefully open HV-91000 to release BCP upstream piping via the same path through the skid by HV-91202.

6) Close PV-91000 from Aerzen HMI.

7) Open BCP discharge valve HV-91200 and HV-21100 at HP inlet.

8) Fill BCP skid with clean helium from Aux line via HV-91006 at BCP inlet to MP pressure (~3…4 bara at PI-91000 and not rising) and close again.

9) Proceed with valve setting as per permissive page for selected compressor operation mode.

10) Operator acknowledge BCP suction line vented to oily SV relief header and BCP filled to MP.</text>
    <x>26</x>
    <y>77</y>
    <width>804</width>
    <height>363</height>
    <font>
      <font name="Normal" family="sans" style="REGULAR" size="11.881188118811881">
      </font>
    </font>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_123</name>
    <text>Suction Line Venting Steps</text>
    <x>11</x>
    <y>50</y>
    <width>230</width>
    <height>30</height>
    <font>
      <font name="Header 3" family="Source Sans Pro" style="BOLD_ITALIC" size="18.0">
      </font>
    </font>
    <foreground_color>
      <color red="0" green="0" blue="0">
      </color>
    </foreground_color>
    <background_color>
      <color red="255" green="255" blue="255">
      </color>
    </background_color>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <actions>
    </actions>
    <border_color>
      <color red="0" green="128" blue="255">
      </color>
    </border_color>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="write_pv">
        <pv_name>CrS-ACCP:SC-FSM-91000:AckBcpSuctionLine</pv_name>
        <value>1</value>
        <description>WritePV</description>
      </action>
    </actions>
    <text>Operator Acknowledge</text>
    <x>370</x>
    <y>450</y>
    <width>130</width>
    <height>60</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LED</name>
    <pv_name>CrS-ACCP:SC-FSM-91000:AckBcpSuctionLine-RB</pv_name>
    <x>650</x>
    <y>490</y>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group</name>
    <x>750</x>
    <y>520</y>
    <width>118</width>
    <height>46</height>
    <style>3</style>
    <transparent>true</transparent>
    <rules>
      <rule name="Enable" prop_id="visible" out_exp="false">
        <exp bool_exp="pvInt0 == 1">
          <value>true</value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>false</value>
        </exp>
        <pv_name>CrS-ACCP:SC-FSM-91000:AckBcpSuctionLine-RB</pv_name>
      </rule>
    </rules>
    <widget type="label" version="2.0.0">
      <name>Label_124</name>
      <text>Close</text>
      <width>118</width>
      <height>46</height>
      <font>
        <font name="SMALL-SANS-PLAIN" family="Source Sans Pro" style="REGULAR" size="14.0">
        </font>
      </font>
      <foreground_color>
        <color red="0" green="0" blue="0">
        </color>
      </foreground_color>
      <background_color>
        <color red="255" green="255" blue="255">
        </color>
      </background_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
      <wrap_words>false</wrap_words>
      <actions>
        <action type="open_display">
          <file>../../processCCString.opi</file>
          <target>replace</target>
          <description>Open Display</description>
        </action>
      </actions>
      <border_width>1</border_width>
    </widget>
    <widget type="action_button" version="3.0.0">
      <name>Action Button_2</name>
      <actions>
        <action type="execute">
          <script file="../../../Common/Scripts/closeOpi.js">
          </script>
          <description>Close OPI</description>
        </action>
      </actions>
      <text></text>
      <width>118</width>
      <height>46</height>
      <transparent>true</transparent>
      <tooltip>$(actions)</tooltip>
    </widget>
  </widget>
</display>
