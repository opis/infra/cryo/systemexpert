<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-03-20 08:35:34 by attilahorvath-->
<display version="2.0.0">
  <name>StateMachine_BlockIcon_Standard</name>
  <width>408</width>
  <height>215</height>
  <background_color>
    <color red="255" green="255" blue="255" alpha="0">
    </color>
  </background_color>
  <widget type="rectangle" version="2.0.0">
    <name>MainBackRectangle</name>
    <width>402</width>
    <height>210</height>
    <line_color>
      <color name="BLUE-BORDER" red="47" green="135" blue="148">
      </color>
    </line_color>
    <background_color>
      <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
    <rules>
      <rule name="BackgroundColor" prop_id="background_color" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>
            <color name="GREEN" red="61" green="216" blue="61">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt1 == 0">
          <value>
            <color name="DISCONNECTED" red="105" green="77" blue="164">
            </color>
          </value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>
            <color name="Button_Background" red="236" green="236" blue="236">
            </color>
          </value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:STS_Active</pv_name>
        <pv_name>${PLCName}:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_TitleLBL</name>
    <text>${WIDIndex}</text>
    <x>111</x>
    <width>189</width>
    <height>21</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <background_color>
      <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
      </color>
    </background_color>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <tooltip>Step Name</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WIDStringMessage1</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:StrMsg1</pv_name>
    <x>8</x>
    <y>33</y>
    <width>384</width>
    <height>26</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="14.0">
      </font>
    </font>
    <transparent>true</transparent>
    <precision>2</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt;0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>${PLCName}:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
    <tooltip>Step Message</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="symbol" version="2.0.0">
    <name>WID_AlarmIcon</name>
    <symbols>
      <symbol>../../CommonSymbols/error@32.png</symbol>
    </symbols>
    <x>4</x>
    <y>3</y>
    <width>36</width>
    <height>30</height>
    <actions>
    </actions>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == true">
          <value>true</value>
        </exp>
        <exp bool_exp="pv0 == false">
          <value>false</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:GroupAlarm</pv_name>
      </rule>
    </rules>
    <tooltip>Alarm event occured!</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_StringMessage2</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:StrMsg2</pv_name>
    <x>8</x>
    <y>59</y>
    <width>385</width>
    <height>26</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="14.0">
      </font>
    </font>
    <transparent>true</transparent>
    <precision>2</precision>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt;0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>${PLCName}:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
    <tooltip>Step Message</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_width>1</border_width>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>BottomRectangle</name>
    <x>3</x>
    <y>89</y>
    <width>396</width>
    <height>121</height>
    <line_color>
      <color name="BLUE-BORDER" red="47" green="135" blue="148">
      </color>
    </line_color>
    <background_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>Status:</text>
    <x>78</x>
    <y>91</y>
    <width>80</width>
    <height>23</height>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_1</name>
    <text>Not Active</text>
    <x>131</x>
    <y>91</y>
    <width>258</width>
    <height>23</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <vertical_alignment>1</vertical_alignment>
    <rules>
      <rule name="TextRule" prop_id="text" out_exp="false">
        <exp bool_exp="pv0 == 1">
          <value>ACTIVE</value>
        </exp>
        <exp bool_exp="pv0 == 0">
          <value>NOT ACTIVE</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:STS_Active</pv_name>
      </rule>
    </rules>
    <tooltip>Sate Status</tooltip>
  </widget>
  <widget type="textupdate" version="2.0.0">
    <name>WID_StepID</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:STS_StateID</pv_name>
    <x>367</x>
    <width>26</width>
    <height>26</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="14.0">
      </font>
    </font>
    <transparent>true</transparent>
    <precision>0</precision>
    <vertical_alignment>1</vertical_alignment>
    <wrap_words>false</wrap_words>
    <rules>
      <rule name="Disconnected" prop_id="background_color" out_exp="false">
        <exp bool_exp="pvInt0 &gt;0">
          <value>
            <color name="Read_Background" red="230" green="235" blue="232">
            </color>
          </value>
        </exp>
        <exp bool_exp="pvInt0 == 0">
          <value>
            <color name="INVALID" red="149" green="110" blue="221">
            </color>
          </value>
        </exp>
        <pv_name>${PLCName}:PLCHashCorrectR</pv_name>
      </rule>
    </rules>
    <tooltip>Step Message</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <border_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_3</name>
    <text>ID:</text>
    <x>348</x>
    <y>3</y>
    <width>21</width>
    <height>23</height>
    <font>
      <font family="Source Sans Pro" style="REGULAR" size="14.0">
      </font>
    </font>
    <foreground_color>
      <color name="TEXT-DARK" red="25" green="25" blue="25">
      </color>
    </foreground_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>WID_TitleLBL_1</name>
    <text>${StateName}</text>
    <x>9</x>
    <y>12</y>
    <width>386</width>
    <height>21</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="16.0">
      </font>
    </font>
    <foreground_color>
      <color name="TEXT" red="25" green="25" blue="25">
      </color>
    </foreground_color>
    <background_color>
      <color name="BLUE-BACKGROUND" red="195" green="226" blue="225">
      </color>
    </background_color>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
    <tooltip>Step Name</tooltip>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>BTN_Start Button</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Activate</description>
      </action>
    </actions>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:CMD_Activate</pv_name>
    <text>ACTIVATE</text>
    <x>107</x>
    <y>160</y>
    <width>178</width>
    <height>36</height>
    <font>
      <font family="Source Sans Pro" style="BOLD" size="14.0">
      </font>
    </font>
    <rules>
      <rule name="Enabled Rule" prop_id="enabled" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:ENA_Activate</pv_name>
      </rule>
      <rule name="Visibility Rule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:Vis_Activate</pv_name>
      </rule>
    </rules>
    <tooltip>Activate this Step Manually</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
    <show_confirm_dialog>true</show_confirm_dialog>
    <confirm_message>Are your sure you want to Activate this State?</confirm_message>
  </widget>
  <widget type="label" version="2.0.0">
    <name>Label_11</name>
    <text>Activation Criteria:</text>
    <x>78</x>
    <y>116</y>
    <width>215</width>
    <height>26</height>
    <foreground_color>
      <color name="WHITE" red="255" green="255" blue="255">
      </color>
    </foreground_color>
  </widget>
  <widget type="led" version="2.0.0">
    <name>LED_3</name>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:STS_RequisitsOK</pv_name>
    <x>292</x>
    <y>118</y>
    <width>25</width>
    <height>25</height>
    <tooltip>Interlocked Devices in this FSM are in position</tooltip>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>Rectangle_2</name>
    <x>14</x>
    <y>144</y>
    <width>371</width>
    <height>4</height>
    <line_color>
      <color name="LED-YELLOW-OFF" red="110" green="108" blue="90">
      </color>
    </line_color>
    <background_color>
      <color name="BLACK-BORDER" red="121" green="121" blue="121">
      </color>
    </background_color>
  </widget>
  <widget type="rectangle" version="2.0.0">
    <name>OperatorAction_Background</name>
    <width>408</width>
    <height>215</height>
    <visible>false</visible>
    <line_color>
      <color name="BLUE-BORDER" red="47" green="135" blue="148">
      </color>
    </line_color>
    <background_color>
      <color name="GRAY-BORDER" red="121" green="121" blue="121">
      </color>
    </background_color>
    <corner_width>10</corner_width>
    <corner_height>10</corner_height>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0==0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0==1">
          <value>true</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:FLAG_OperatorRq</pv_name>
      </rule>
    </rules>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>OpAction_YES</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Write PV</description>
      </action>
    </actions>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:CMD_YES</pv_name>
    <text>YES</text>
    <x>55</x>
    <y>151</y>
    <width>117</width>
    <height>52</height>
    <visible>false</visible>
    <font>
      <font family="Source Code Pro" style="BOLD" size="21.0">
      </font>
    </font>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0==0 || pv1 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0==1 &amp;&amp; pv1==1">
          <value>true</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:FLAG_OperatorRq</pv_name>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:BTN_YES_ENA</pv_name>
      </rule>
    </rules>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>OpAction_OK</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Write PV</description>
      </action>
    </actions>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:CMD_OK</pv_name>
    <text>OK</text>
    <x>142</x>
    <y>151</y>
    <width>118</width>
    <height>52</height>
    <visible>false</visible>
    <font>
      <font family="Source Code Pro" style="BOLD" size="21.0">
      </font>
    </font>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0==0 || pv1 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0==1 &amp;&amp; pv1==1">
          <value>true</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:FLAG_OperatorRq</pv_name>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:BTN_OK_ENA</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>OpAction_NO</name>
    <actions>
      <action type="write_pv">
        <pv_name>$(pv_name)</pv_name>
        <value>1</value>
        <description>Write PV</description>
      </action>
    </actions>
    <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:CMD_NO</pv_name>
    <text>NO</text>
    <x>226</x>
    <y>151</y>
    <width>118</width>
    <height>52</height>
    <visible>false</visible>
    <font>
      <font family="Source Code Pro" style="BOLD" size="21.0">
      </font>
    </font>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0==0 || pv1 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0==1 &amp;&amp; pv1==1">
          <value>true</value>
        </exp>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:FLAG_OperatorRq</pv_name>
        <pv_name>${WIDSecSub}:${WIDDis}-${WIDDev}-${WIDIndex}:BTN_NO_ENA</pv_name>
      </rule>
    </rules>
    <tooltip>$(actions)</tooltip>
    <border_alarm_sensitive>false</border_alarm_sensitive>
  </widget>
</display>
