<?xml version="1.0" encoding="UTF-8"?>
<databrowser>
  <title>PT-42480</title>
  <show_toolbar>true</show_toolbar>
  <update_period>3.0</update_period>
  <scroll_step>5</scroll_step>
  <scroll>true</scroll>
  <start>-9 hours -21 minutes</start>
  <end>now</end>
  <archive_rescale>NONE</archive_rescale>
  <foreground>
    <red>229</red>
    <green>229</green>
    <blue>229</blue>
  </foreground>
  <background>
    <red>77</red>
    <green>77</green>
    <blue>77</blue>
  </background>
  <title_font>Cantarell|16|1</title_font>
  <label_font>Cantarell|11|1</label_font>
  <scale_font>Cantarell|10|0</scale_font>
  <legend_font>Cantarell|10|0</legend_font>
  <axes>
    <axis>
      <visible>true</visible>
      <name>Value Axis</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>true</use_trace_names>
      <right>false</right>
      <color>
        <red>0</red>
        <green>255</green>
        <blue>0</blue>
      </color>
      <min>0.0</min>
      <max>100.0</max>
      <grid>true</grid>
      <autoscale>false</autoscale>
      <log_scale>false</log_scale>
    </axis>
    <axis>
      <visible>false</visible>
      <name>Value 1</name>
      <use_axis_name>false</use_axis_name>
      <use_trace_names>true</use_trace_names>
      <right>false</right>
      <color>
        <red>255</red>
        <green>153</green>
        <blue>102</blue>
      </color>
      <min>0.0</min>
      <max>100.0</max>
      <grid>false</grid>
      <autoscale>false</autoscale>
      <log_scale>false</log_scale>
    </axis>
  </axes>
  <annotations>
  </annotations>
  <pvlist>
    <pv>
      <display_name>PT-42480</display_name>
      <visible>true</visible>
      <name>CrS-CMS:Cryo-PT-42480:MeasValue</name>
      <axis>0</axis>
      <color>
        <red>0</red>
        <green>255</green>
        <blue>0</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>3</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>15000</ring_size>
      <request>OPTIMIZED</request>
      <archive>
        <name>pbraw://localhost:17668/retrieval</name>
        <url>pbraw://localhost:17668/retrieval</url>
        <key>1</key>
      </archive>
    </pv>
    <pv>
      <display_name>CrS-CMS:Cryo-PT-42993:MeasValue</display_name>
      <visible>false</visible>
      <name>CrS-CMS:Cryo-PT-42993:MeasValue</name>
      <axis>1</axis>
      <color>
        <red>230</red>
        <green>153</green>
        <blue>77</blue>
      </color>
      <trace_type>AREA</trace_type>
      <linewidth>2</linewidth>
      <line_style>SOLID</line_style>
      <point_type>NONE</point_type>
      <point_size>2</point_size>
      <waveform_index>0</waveform_index>
      <period>0.0</period>
      <ring_size>5000</ring_size>
      <request>OPTIMIZED</request>
    </pv>
  </pvlist>
</databrowser>
