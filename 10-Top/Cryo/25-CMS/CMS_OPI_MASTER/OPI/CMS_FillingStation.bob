<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-09-18 10:26:28 by attilahorvath-->
<display version="2.0.0">
  <name>Hydrogen Filling Station</name>
  <macros>
    <PLCName>Tgt-THHeFS:Ctrl-PLC-001</PLCName>
  </macros>
  <width>2557</width>
  <height>1285</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>Filling Station</text>
    <y>90</y>
    <width>2557</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color red="0" green="148" blue="202">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="picture" version="2.0.0">
    <name>Background</name>
    <file>../OPIBackGrounds/FillingStation_transparent_background.png</file>
    <y>120</y>
    <width>2557</width>
    <height>1165</height>
    <stretch_image>true</stretch_image>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>CMS_THFS_Header.bob</file>
    <width>2557</width>
    <height>90</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>PT-62610</name>
    <macros>
      <Address>KF33 %IW338</Address>
      <WIDDev>PT</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>62610</WIDIndex>
      <WIDLabel>Pressure</WIDLabel>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../99-Shared/AnalogTransmitters/blockicons/AnalogTransmitterGeneric_BlockIcon_OnlyText_Vertical_Compact.bob</file>
    <x>696</x>
    <y>314</y>
    <width>110</width>
    <height>45</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>PT-62620</name>
    <macros>
      <Address>KF33 %IW338</Address>
      <WIDDev>PT</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>62620</WIDIndex>
      <WIDLabel>Pressure</WIDLabel>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../99-Shared/AnalogTransmitters/blockicons/AnalogTransmitterGeneric_BlockIcon_OnlyText_Vertical_Compact.bob</file>
    <x>696</x>
    <y>661</y>
    <width>110</width>
    <height>45</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>PT-62320</name>
    <macros>
      <Address>KF33 %IW338</Address>
      <WIDDev>PT</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>62320</WIDIndex>
      <WIDLabel>Pressure</WIDLabel>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../99-Shared/AnalogTransmitters/blockicons/AnalogTransmitterGeneric_BlockIcon_OnlyText_Vertical_Compact.bob</file>
    <x>1415</x>
    <y>314</y>
    <width>110</width>
    <height>45</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>PT-66000</name>
    <macros>
      <Address>KF33 %IW338</Address>
      <WIDDev>PT</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>66000</WIDIndex>
      <WIDLabel>Pressure</WIDLabel>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../99-Shared/AnalogTransmitters/blockicons/AnalogTransmitterGeneric_BlockIcon_OnlyText_Vertical_Compact.bob</file>
    <x>797</x>
    <y>949</y>
    <width>110</width>
    <height>45</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>PV-62500</name>
    <macros>
      <WIDDev>PV</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>62500</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../99-Shared/Valves/valve solenoid/blockicons/PV_VALVE_BlockIcon_Vertical_Left_Compact.bob</file>
    <x>1805</x>
    <y>272</y>
    <width>175</width>
    <height>99</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>PV-62350</name>
    <macros>
      <WIDDev>PV</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>62350</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../99-Shared/Valves/valve solenoid/blockicons/PV_VALVE_BlockIcon_Horizontal_Compact.bob</file>
    <x>2008</x>
    <y>311</y>
    <width>100</width>
    <height>130</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>PV-62300</name>
    <macros>
      <WIDDev>PV</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>62300</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../99-Shared/Valves/valve solenoid/blockicons/PV_VALVE_BlockIcon_Horizontal_Compact.bob</file>
    <x>1121</x>
    <y>315</y>
    <width>100</width>
    <height>130</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="action_button" version="3.0.0">
    <name>Action Button</name>
    <actions>
      <action type="open_display">
        <file>HeFilling/He_FillingStation.bob</file>
        <target>window</target>
        <description>Open Display</description>
      </action>
    </actions>
    <text>Pristine Helium OPI</text>
    <x>2350</x>
    <y>130</y>
    <width>190</width>
    <height>40</height>
    <tooltip>$(actions)</tooltip>
  </widget>
  <widget type="group" version="3.0.0">
    <name>Group</name>
    <x>1540</x>
    <y>490</y>
    <width>730</width>
    <height>640</height>
    <style>3</style>
    <transparent>true</transparent>
    <widget type="rectangle" version="2.0.0">
      <name>Rectangle</name>
      <width>730</width>
      <height>640</height>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>Embedded Display_1</name>
      <macros>
        <WIDDev>FSM</WIDDev>
        <WIDDis>SC</WIDDis>
        <WIDIndex>001</WIDIndex>
        <WIDSecSub>Tgt-THHeFS</WIDSecSub>
        <StateName>Maintenance</StateName>
      </macros>
      <file>../../99-Shared/StateMachines/blockicons/THFS_StateMachine_BlockIcon_Standard_Compact.bob</file>
      <x>71</x>
      <y>70</y>
      <width>300</width>
      <height>160</height>
      <resize>3</resize>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>Embedded Display_2</name>
      <macros>
        <WIDDev>FSM</WIDDev>
        <WIDDis>SC</WIDDis>
        <WIDIndex>002</WIDIndex>
        <WIDSecSub>Tgt-THHeFS</WIDSecSub>
        <StateName>StandBy</StateName>
      </macros>
      <file>../../99-Shared/StateMachines/blockicons/THFS_StateMachine_BlockIcon_Standard_Compact.bob</file>
      <x>71</x>
      <y>270</y>
      <width>300</width>
      <height>160</height>
      <resize>3</resize>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>Embedded Display_3</name>
      <macros>
        <WIDDev>FSM</WIDDev>
        <WIDDis>SC</WIDDis>
        <WIDIndex>003</WIDIndex>
        <WIDSecSub>Tgt-THHeFS</WIDSecSub>
        <StateName>Filling On</StateName>
      </macros>
      <file>../../99-Shared/StateMachines/blockicons/THFS_StateMachine_BlockIcon_Standard_Compact.bob</file>
      <x>71</x>
      <y>470</y>
      <width>300</width>
      <height>160</height>
      <resize>3</resize>
    </widget>
    <widget type="embedded" version="2.0.0">
      <name>Embedded Display_4</name>
      <macros>
        <WIDDev>FSM</WIDDev>
        <WIDDis>SC</WIDDis>
        <WIDIndex>004</WIDIndex>
        <WIDSecSub>Tgt-THHeFS</WIDSecSub>
        <StateName>Venting</StateName>
      </macros>
      <file>../../99-Shared/StateMachines/blockicons/THFS_StateMachine_BlockIcon_Standard_Compact.bob</file>
      <x>411</x>
      <y>70</y>
      <width>300</width>
      <height>160</height>
      <resize>3</resize>
    </widget>
    <widget type="picture" version="2.0.0">
      <name>Picture</name>
      <file>../OPIBackGrounds/FillingStateMachine.png</file>
      <x>20</x>
      <y>139</y>
      <width>548</width>
      <height>419</height>
    </widget>
    <widget type="rectangle" version="2.0.0">
      <name>Rectangle_1</name>
      <width>730</width>
      <height>50</height>
      <line_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </line_color>
      <background_color>
        <color name="GROUP-BORDER" red="150" green="155" blue="151">
        </color>
      </background_color>
      <corner_width>10</corner_width>
      <corner_height>10</corner_height>
    </widget>
    <widget type="label" version="2.0.0">
      <name>LB_Status</name>
      <text>STATE MACHINE</text>
      <width>730</width>
      <height>50</height>
      <font>
        <font name="Header 2" family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
        </font>
      </font>
      <foreground_color>
        <color name="GRAY-TEXT" red="255" green="255" blue="255">
        </color>
      </foreground_color>
      <background_color>
        <color name="GROUP-BACKGROUND" red="200" green="205" blue="201">
        </color>
      </background_color>
      <horizontal_alignment>1</horizontal_alignment>
      <vertical_alignment>1</vertical_alignment>
    </widget>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>PSS_Signals</name>
    <macros>
      <PLCName>Tgt-THHeFS:Ctrl-PLC-001</PLCName>
    </macros>
    <file>../Diagnostics/CMS_PSS_Signals.bob</file>
    <x>2310</x>
    <y>640</y>
    <width>235</width>
    <height>320</height>
  </widget>
</display>
