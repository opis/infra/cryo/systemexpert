<?xml version="1.0" encoding="UTF-8"?>
<display version="2.0.0">
  <name>CMS Y10 - CV-42320 Control</name>
  <macros>
    <PLCName>CrS-CMS:Ctrl-PLC-001</PLCName>
  </macros>
  <width>1780</width>
  <height>1100</height>
  <background_color>
    <color name="WHITE" red="255" green="255" blue="255">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>Y2 - CV-62029 and Heaters Control</text>
    <y>90</y>
    <width>1780</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color red="0" green="148" blue="202">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel</name>
    <text>State Flag Status</text>
    <x>810</x>
    <y>134</y>
    <width>570</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel</name>
    <text>State Block Icon</text>
    <x>1410</x>
    <y>136</y>
    <width>330</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1</name>
    <macros>
      <Description>CV-42320 finished</Description>
      <FlagID>Flag1</FlagID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>175</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ControlledDevicesLabel</name>
    <text>Controlled / Measured Devices</text>
    <x>30</x>
    <y>560</y>
    <width>1710</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>PT-62073</name>
    <macros>
      <WIDDev>PT</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>62073</WIDIndex>
      <WIDLabel>Pressure</WIDLabel>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../99-Shared/AnalogTransmitters/blockicons/AnalogTransmitter_BlockIcon_OnlyText_Vertical_Compact.bob</file>
    <x>30</x>
    <y>610</y>
    <width>114</width>
    <height>48</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="label" version="2.0.0">
    <name>LBL_DevicesInManual</name>
    <text>Warning: some controlled devices are in Manual!</text>
    <x>30</x>
    <y>500</y>
    <width>501</width>
    <height>36</height>
    <font>
      <font family="Source Sans Pro Semibold" style="REGULAR" size="24.0">
      </font>
    </font>
    <foreground_color>
      <color name="RAL-2000" red="213" green="111" blue="1">
      </color>
    </foreground_color>
    <horizontal_alignment>1</horizontal_alignment>
    <auto_size>true</auto_size>
    <rules>
      <rule name="VisibilityRule" prop_id="visible" out_exp="false">
        <exp bool_exp="pv0 == 0">
          <value>false</value>
        </exp>
        <exp bool_exp="pv0 == 1">
          <value>true</value>
        </exp>
        <pv_name>${StepDeviceName}:DevInManual</pv_name>
      </rule>
    </rules>
    <border_width>3</border_width>
    <border_color>
      <color name="ORANGE" red="254" green="194" blue="81">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel_1</name>
    <text>Parallel State Parameters</text>
    <x>30</x>
    <y>134</y>
    <width>759</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_1</name>
    <macros>
      <Description>Failure Action Active</Description>
      <FlagID>Flag2</FlagID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Flag_Template.bob</file>
    <x>810</x>
    <y>210</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>CV_62029</name>
    <macros>
      <WIDDev>CV</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>62029</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../99-Shared/Valves/valve analog/blockicons/CV_ControlVALVE_BlockIcon_Horizontal_Compact.bob</file>
    <x>190</x>
    <y>610</y>
    <width>100</width>
    <height>130</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>../CMS_Header.bob</file>
    <width>2080</width>
    <height>90</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>EH-621256</name>
    <macros>
      <WIDDev>PID</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>621256</WIDIndex>
      <WIDLabel>Heater</WIDLabel>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../99-Shared/Heaters/blockicons/EH_HeaterSupply_PID_Analog_BlockIcon_OnlyText_Vertical_Compact.bob</file>
    <x>497</x>
    <y>612</y>
    <width>133</width>
    <height>108</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>EH-621212</name>
    <macros>
      <WIDDev>PID</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>621212</WIDIndex>
      <WIDLabel>Heater</WIDLabel>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../99-Shared/Heaters/blockicons/EH_HeaterSupply_PID_Analog_BlockIcon_OnlyText_Vertical_Compact.bob</file>
    <x>497</x>
    <y>720</y>
    <width>133</width>
    <height>120</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>EH-621234</name>
    <macros>
      <WIDDev>PID</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>621234</WIDIndex>
      <WIDLabel>Heater</WIDLabel>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../99-Shared/Heaters/blockicons/EH_HeaterSupply_PID_Analog_BlockIcon_OnlyText_Vertical_Compact.bob</file>
    <x>340</x>
    <y>720</y>
    <width>133</width>
    <height>120</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>EH-621278</name>
    <macros>
      <WIDDev>PID</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>621278</WIDIndex>
      <WIDLabel>Heater</WIDLabel>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../99-Shared/Heaters/blockicons/EH_HeaterSupply_PID_Analog_BlockIcon_OnlyText_Vertical_Compact.bob</file>
    <x>340</x>
    <y>610</y>
    <width>133</width>
    <height>108</height>
    <resize>1</resize>
    <transparent>true</transparent>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>TT-62109</name>
    <macros>
      <SensorA>TT-62109</SensorA>
      <SensorB>TT-62110</SensorB>
      <WIDDev>TT</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>62109</WIDIndex>
      <WIDLabel>Temperature</WIDLabel>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../99-Shared/AnalogTransmitters/blockicons/AnalogTransmitter_BlockIcon_LakeShore_Redundant_OnlyText_Vertical_Compact.bob</file>
    <x>690</x>
    <y>612</y>
    <width>155</width>
    <height>65</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel_2</name>
    <text>Parallel Function Internal Alarms</text>
    <x>810</x>
    <y>380</y>
    <width>570</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_2</name>
    <macros>
      <AlarmID>Alarm1</AlarmID>
      <Description>All parameters are zero!</Description>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_AlarmMessage_Template.bob</file>
    <x>810</x>
    <y>424</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Y9</name>
    <macros>
      <Faceplate>../../../CMS_OPI_MASTER/OPI/States/CMS_ParallelFaceplate_Y10.bob</Faceplate>
      <StepName>CV-42320 Control</StepName>
      <WIDDev>Virt</WIDDev>
      <WIDDis>Cryo</WIDDis>
      <WIDIndex>Y10</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_BlockIcon_Parallel_Compact.bob</file>
    <x>1410</x>
    <y>175</y>
    <width>330</width>
    <height>130</height>
    <resize>2</resize>
  </widget>
</display>
