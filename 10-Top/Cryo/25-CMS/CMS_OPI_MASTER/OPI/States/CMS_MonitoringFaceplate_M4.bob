<?xml version="1.0" encoding="UTF-8"?>
<!--Saved on 2024-11-07 15:18:24 by attilahorvath-->
<display version="2.0.0">
  <name>CMS M4 Hydrogen Leak Monitoring</name>
  <macros>
    <PLCName>CrS-CMS:Ctrl-PLC-001</PLCName>
    <StateDeviceName>CrS-CMS:SC-FSM-004m</StateDeviceName>
  </macros>
  <width>2557</width>
  <height>1165</height>
  <background_color>
    <color name="BACKGROUND" red="220" green="225" blue="221">
    </color>
  </background_color>
  <grid_color>
    <color name="TEXT-LIGHT" red="230" green="230" blue="230">
    </color>
  </grid_color>
  <widget type="label" version="2.0.0">
    <name>Label</name>
    <text>M4 - Hydrogen Monitoring</text>
    <y>90</y>
    <width>2557</width>
    <height>30</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <foreground_color>
      <color red="255" green="255" blue="255">
      </color>
    </foreground_color>
    <background_color>
      <color red="0" green="148" blue="202">
      </color>
    </background_color>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <vertical_alignment>1</vertical_alignment>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel</name>
    <text>State Flag Status</text>
    <x>1594</x>
    <y>150</y>
    <width>570</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>BlkIconLabel</name>
    <text>Block Icons</text>
    <x>2200</x>
    <y>150</y>
    <width>330</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ParameterLabel</name>
    <text>State Parameters</text>
    <x>216</x>
    <y>150</y>
    <width>759</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1</name>
    <macros>
      <Description>10% LEL Failure Action Delay Timer</Description>
      <EngUnit>sec</EngUnit>
      <ParameterID>S1</ParameterID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Paramter_Template.bob</file>
    <x>216</x>
    <y>190</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>ControlledDevicesLabel</name>
    <text>Measurement</text>
    <y>150</y>
    <width>190</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_1</name>
    <macros>
      <Description>Hydrogen 10% LEL Reached</Description>
      <WarningID>Warning1</WarningID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_WarningMessage_Template.bob</file>
    <x>999</x>
    <y>190</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Embedded Display</name>
    <file>../CMS_Header.bob</file>
    <width>2557</width>
    <height>90</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_2</name>
    <macros>
      <AlarmID>Alarm1</AlarmID>
      <Description>Hydrogen Leak 10% LEL</Description>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_AlarmMessage_Template.bob</file>
    <x>999</x>
    <y>295</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_3</name>
    <macros>
      <AlarmID>Alarm2</AlarmID>
      <Description>Hydrogen Leak 25% LEL</Description>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_AlarmMessage_Template.bob</file>
    <x>999</x>
    <y>330</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>WDF5</name>
    <macros>
      <Faceplate>../../../CMS_OPI_MASTER/OPI/States/CMS_MonitoringFaceplate_M4.bob</Faceplate>
      <StateName>Hydrogen Leak Monitoring</StateName>
      <WIDDev>FSM</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>004m</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_BlockIcon_MonitoringFunction_Compact.bob</file>
    <x>2200</x>
    <y>189</y>
    <width>330</width>
    <height>160</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>WDF5_2</name>
    <macros>
      <Faceplate>../../../CMS_OPI_MASTER/OPI/States/CMS_MonitoringFaceplate_F8.bob</Faceplate>
      <StateName>Multiple Faulure Action</StateName>
      <WIDDev>FSM</WIDDev>
      <WIDDis>SC</WIDDis>
      <WIDIndex>008f</WIDIndex>
      <WIDSecSub>CrS-CMS</WIDSecSub>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_BlockIcon_FailureAction_Compact.bob</file>
    <x>2200</x>
    <y>349</y>
    <width>330</width>
    <height>160</height>
    <resize>2</resize>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel_4</name>
    <text>Process Warning/Alarms</text>
    <x>999</x>
    <y>150</y>
    <width>570</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="label" version="2.0.0">
    <name>FlagLabel_5</name>
    <text>Monitoring Function Internal Warnings</text>
    <x>1594</x>
    <y>373</y>
    <width>570</width>
    <height>32</height>
    <font>
      <font family="Source Sans Pro" style="BOLD_ITALIC" size="21.0">
      </font>
    </font>
    <transparent>false</transparent>
    <horizontal_alignment>1</horizontal_alignment>
    <border_width>1</border_width>
    <border_color>
      <color name="WHITE-BORDER" red="121" green="121" blue="121">
      </color>
    </border_color>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_4</name>
    <macros>
      <AlarmID>Alarm3</AlarmID>
      <Description>Gas Detection System Failure</Description>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_AlarmMessage_Template.bob</file>
    <x>999</x>
    <y>365</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_5</name>
    <macros>
      <Description>Hydrogen 25% LEL Reached</Description>
      <WarningID>Warning2</WarningID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_WarningMessage_Template.bob</file>
    <x>999</x>
    <y>225</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Flag1_6</name>
    <macros>
      <Description>Gas Detection System Special State</Description>
      <WarningID>Warning3</WarningID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_WarningMessage_Template.bob</file>
    <x>999</x>
    <y>260</y>
    <width>585</width>
    <height>35</height>
    <resize>1</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1_1</name>
    <macros>
      <Description>25% LEL Failure Action Delay Timer</Description>
      <EngUnit>sec</EngUnit>
      <ParameterID>S2</ParameterID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Paramter_Template.bob</file>
    <x>216</x>
    <y>220</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>Parameter1_2</name>
    <macros>
      <Description>Special State Failure Action Delay Timer</Description>
      <EngUnit>sec</EngUnit>
      <ParameterID>S3</ParameterID>
    </macros>
    <file>../../../99-Shared/StateMachines/blockicons/StateMachine_Paramter_Template.bob</file>
    <x>216</x>
    <y>250</y>
    <width>760</width>
    <height>30</height>
    <resize>2</resize>
  </widget>
  <widget type="embedded" version="2.0.0">
    <name>PSS_Signals</name>
    <file>../../Diagnostics/CMS_PSS_Signals.bob</file>
    <y>191</y>
    <width>200</width>
    <height>289</height>
    <resize>1</resize>
  </widget>
</display>
