
# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

# ValveNames
cvList = ["MBL-010CDL:Cryo-CV-814%s:%s_Setpoint",\
"MBL-020CDL:Cryo-CV-815%s:%s_Setpoint", \
"MBL-030CDL:Cryo-CV-816%s:%s_Setpoint", \
"MBL-040CDL:Cryo-CV-817%s:%s_Setpoint", \
"MBL-050CDL:Cryo-CV-818%s:%s_Setpoint", \
"MBL-060CDL:Cryo-CV-819%s:%s_Setpoint", \
"MBL-070CDL:Cryo-CV-820%s:%s_Setpoint", \
"MBL-080CDL:Cryo-CV-821%s:%s_Setpoint", \
"MBL-090CDL:Cryo-CV-822%s:%s_Setpoint", \
"HBL-010CDL:Cryo-CV-823%s:%s_Setpoint", \
"HBL-020CDL:Cryo-CV-824%s:%s_Setpoint", \
"HBL-030CDL:Cryo-CV-825%s:%s_Setpoint", \
"HBL-040CDL:Cryo-CV-826%s:%s_Setpoint", \
"HBL-050CDL:Cryo-CV-827%s:%s_Setpoint", \
"HBL-060CDL:Cryo-CV-828%s:%s_Setpoint", \
"HBL-070CDL:Cryo-CV-829%s:%s_Setpoint", \
"HBL-080CDL:Cryo-CV-830%s:%s_Setpoint", \
"HBL-090CDL:Cryo-CV-831%s:%s_Setpoint", \
"HBL-100CDL:Cryo-CV-832%s:%s_Setpoint", \
"HBL-110CDL:Cryo-CV-833%s:%s_Setpoint", \
"HBL-120CDL:Cryo-CV-834%s:%s_Setpoint", \
"HBL-130CDL:Cryo-CV-835%s:%s_Setpoint", \
"HBL-140CDL:Cryo-CV-836%s:%s_Setpoint", \
"HBL-150CDL:Cryo-CV-837%s:%s_Setpoint", \
"HBL-160CDL:Cryo-CV-838%s:%s_Setpoint", \
"HBL-170CDL:Cryo-CV-839%s:%s_Setpoint", \
"HBL-180CDL:Cryo-CV-840%s:%s_Setpoint", \
"HBL-190CDL:Cryo-CV-841%s:%s_Setpoint", \
"HBL-200CDL:Cryo-CV-842%s:%s_Setpoint", \
"HBL-210CDL:Cryo-CV-843%s:%s_Setpoint"]

# Read the valve widget and delta wiget
children = widget.getDisplayModel().runtimeChildren()
name_widget = children.getChildByName("elCVName")
value_widget = children.getChildByName("elDeltaMove")

name_pv = ScriptUtil.getPrimaryPV(name_widget)
value_pv = ScriptUtil.getPrimaryPV(value_widget)
 
valveName = PVUtil.getString(name_pv)
deltaValue = PVUtil.getDouble(value_pv)

# Readback: FB
# Setpoint: P
#myPvName =" HBL-210CDL:Cryo-CV-843%s:%s_Setpoint"

for cv in cvList:
	rbName = (cv % (valveName , "FB"))
	setName = (cv % (valveName , "P"))
	try:
		rbValuePV = PVUtil.createPV(rbName, 1000)
	except:
		print("Error creating PV:" + cv)
	
	rbValue = PVUtil.getDouble(rbValuePV)
	
	setValue = rbValue + deltaValue
	if (setValue > 100) :
		setValue = 100
	elif (setValue < 0) :
		setValue = 0
	
	#print(rbName)
	#print(setName)
	#print(rbValue)
	#print(setValue)
	
	try:
		PVUtil.writePV(setName, setValue, 1000)
	except:
		print("Error writing:" + setName + " with value:" + str(setValue))
	PVUtil.releasePV(rbValuePV)
	