
# Embedded python script
from org.csstudio.display.builder.runtime.script import PVUtil, ScriptUtil

# ValveNames
cvList = ["Spk-010CDL:Cryo-CV-%s:%s_Setpoint", \
"Spk-020CDL:Cryo-CV-%s:%s_Setpoint", \
"Spk-030CDL:Cryo-CV-%s:%s_Setpoint", \
"Spk-040CDL:Cryo-CV-%s:%s_Setpoint", \
"Spk-050CDL:Cryo-CV-%s:%s_Setpoint", \
"Spk-060CDL:Cryo-CV-%s:%s_Setpoint", \
"Spk-070CDL:Cryo-CV-%s:%s_Setpoint", \
"Spk-080CDL:Cryo-CV-%s:%s_Setpoint", \
"Spk-090CDL:Cryo-CV-%s:%s_Setpoint", \
"Spk-100CDL:Cryo-CV-%s:%s_Setpoint", \
"Spk-110CDL:Cryo-CV-%s:%s_Setpoint", \
"Spk-120CDL:Cryo-CV-%s:%s_Setpoint", \
"Spk-130CDL:Cryo-CV-%s:%s_Setpoint"]


# Read the valve widget and delta wiget
children = widget.getDisplayModel().runtimeChildren()
name_widget = children.getChildByName("spkCVName")
value_widget = children.getChildByName("spkDeltaMove")

name_pv = ScriptUtil.getPrimaryPV(name_widget)
value_pv = ScriptUtil.getPrimaryPV(value_widget)
 
valveName = PVUtil.getString(name_pv)
deltaValue = PVUtil.getDouble(value_pv)

# Readback: FB
# Setpoint: P
#myPvName =" HBL-210CDL:Cryo-CV-843%s:%s_Setpoint"

for cv in cvList:
	rbName = (cv % (valveName , "FB"))
	setName = (cv % (valveName , "P"))
	try:
		rbValuePV = PVUtil.createPV(rbName, 1000)
	except:
		print("Error creating PV:" + cv)
	
	rbValue = PVUtil.getDouble(rbValuePV)
	setValue = rbValue + deltaValue
	
	if (setValue > 100) :
		setValue = 100
	elif (setValue < 0) :
		setValue = 0
	
	#print(rbName)
	#print(setName)
	#print(rbValue)
	#print(setValue)
	
	try:
		PVUtil.writePV(setName, setValue, 1000)
	except:
		print("Error writing:" + setName + " with value:" + str(setValue))
	PVUtil.releasePV(rbValuePV)
	